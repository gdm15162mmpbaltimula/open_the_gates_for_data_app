jQuery(document).ready(function($){

    if (sessionStorage.getItem('quiz-container') !== 'true') {
        $('body').append("<div id='quiz-container'> <div class='modal-dialog small-quiz'> <div class='modal-content'> <div class='modal-header'><span id='close'><span class='unselectable'>X</span> <h3><span class='label label-warning' id='qid'>2</span>How is this castle called?<br> <img src='images/gr_ghent.jpg' height='100%' width='auto'> </h3> </div> <div class='modal-body'> <div class='col-xs-3 col-xs-offset-5'> <div id='loadbar' style='display: none;'> <div class='blockG' id='rotateG_01'></div> <div class='blockG' id='rotateG_02'></div> <div class='blockG' id='rotateG_03'></div> <div class='blockG' id='rotateG_04'></div> <div class='blockG' id='rotateG_05'></div> <div class='blockG' id='rotateG_06'></div> <div class='blockG' id='rotateG_07'></div> <div class='blockG' id='rotateG_08'></div> </div> </div> <div class='quiz' id='quiz' data-toggle='buttons'> <label class='element-animation1 btn btn-lg btn-danger btn-block'><span class='btn-label'><i class='glyphicon glyphicon-chevron-right'></i></span> <input type='radio' name='q_answer' value='1'>Drakensteen</label> <label class='element-animation2 btn btn-lg btn-danger btn-block'><span class='btn-label'><i class='glyphicon glyphicon-chevron-right'></i></span> <input type='radio' name='q_answer' value='2'>Winterfell</label> <label class='element-animation3 btn btn-lg btn-danger btn-block'><span class='btn-label'><i class='glyphicon glyphicon-chevron-right'></i></span> <input type='radio' name='q_answer' value='3'>The Gravensteen</label> <label class='element-animation4 btn btn-lg btn-danger btn-block'><span class='btn-label'><i class='glyphicon glyphicon-chevron-right'></i></span> <input type='radio' name='q_answer' value='4'>Belfort</label> </div> </div> <div class='modal-footer text-muted'> <span id='answer'></span> </div></span> </div> </div> </div>");
    }


});


$(function(){

    $(document)
    .ajaxStart(function () {
        loading.show();
    }).ajaxStop(function () {
    	loading.hide();
    });

    $("label.btn").bind('click touchstart',function () {
        var choice = $(this).find('input:radio').val();

    	setTimeout(function(){
           $( "#answer" ).html(  $(this).checking(choice) );
            $('#quiz').show();
            $('#loadbar').fadeOut();
           /* something else */
    	}, 0);


        sessionStorage.setItem('quiz-container','true');
    });

    $ans = 3;

    $.fn.checking = function(ck) {
        if (ck != $ans)
            return  "<span class='red'>Wrong, The Gravensteen is a castle in Ghent originating from the Middle Ages. The name means 'castle of the counts' in Dutch. <a title='Here is the map' href='https://goo.gl/maps/z1cChUdbJ5F2' target='_blank'> Here's the map</a></span>";
        else
            return "<span class='green'>Correct, The Gravensteen is a castle in Ghent originating from the Middle Ages. The name means 'castle of the counts' in Dutch.<a title='Here is the map' href='https://goo.gl/maps/z1cChUdbJ5F2' target='_blank'> Here's the map</a></span>";
    };
});

