

var myCenter=new google.maps.LatLng(51.048017, 3.727666);

function initialize()
{
    var mapProp = {
        center:myCenter,
        zoom:11,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);





}

google.maps.event.addDomListener(window, 'load', initialize);
window.onload = function(){



    /* DATUMSGENTFEEST */

    var xmlhttp = new XMLHttpRequest();
    var url = "http://datatank.stad.gent/4/toerisme/visitgentevents.json";

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            myFunction(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

    function myFunction(response) {
        var arr = JSON.parse(response);
        var i;
        var out = "<div class='row'>";

        for(i = 0; i < 21; i++) {
            out += "<div class='col-md-4'><div class='card-columns'><div class='card card-block'><h3 class='card-title'>" +
                arr[i].title +
                "</h3><p class='card-text'>" +
                arr[i].summary + "</p><span class='rood'>" +
                "<span class='taalevent'> " + arr[1].language + "</span><br>" +
                "<span class='openingevent'> " + arr[i].openinghours_short + "</span><br>" +
                "<span class='locatieevent'> " + arr[i].spots[0].title + "</span><br></span>" +

                "</a></div></div></div>";
        }
        out += "</div>";
        document.getElementById("id01").innerHTML = out;
    }

    /* EINDE DATUMSGENTFEEST */



    (function () {
        var App1 = {
            "init": function () {
                this.URLDSGENTFORECAST = 'http://datatank.stad.gent/4/milieuennatuur/weersomstandigheden.json';
                this.gentWeersvoorspellingData = null;
                this.loadGentWeersvoorspelling();
            },
            "loadGentWeersvoorspelling": function () {
                var self = this;
                getJSONByPromise(this.URLDSGENTFORECAST).then(
                    function (data) {
                        self.gentWeersvoorspellingData = data;
                        self.updateUI();
                    },
                    function (status) {
                        console.log(status);
                    }
                );
            },
            "updateUI": function () {
                if (this.gentWeersvoorspellingData != null) {
                    var tempStr = '', gentWeersvoorspelling = null, gentWeersvoorspelling2 = null;
                    tempStr += '<div class="forecast">';

                    gentWeersvoorspelling = this.gentWeersvoorspellingData.properties.attributes[7];
                    gentWeersvoorspelling2 = this.gentWeersvoorspellingData.properties.attributes[0];



                    tempStr += '<span class="weer-beschrijving">' + gentWeersvoorspelling.value + '</span>' + '<br>';
                    tempStr += '<span class="weer-graden">' + gentWeersvoorspelling2.value + '°C</span>' + '<br>';


                    tempStr += '</div>';
                    document.querySelector('#jsondata-forecast').innerHTML = tempStr;
                }
            }
        };
        App1.init();
    })();








    document.getElementById('close').onclick = function(){
        this.parentNode.parentNode.parentNode.parentNode.parentNode
            .removeChild(this.parentNode.parentNode.parentNode.parentNode);
        return false;
    };

};


var xmlhttpparking = new XMLHttpRequest();
var urlparking = "http://datatank.stad.gent/4/mobiliteit/bezettingparkingsrealtime.json";

xmlhttpparking.onreadystatechange=function() {
    if (xmlhttpparking.readyState == 4 && xmlhttpparking.status == 200) {
        functionparking(xmlhttpparking.responseText);
    }
};
xmlhttpparking.open("GET", urlparking, true);
xmlhttpparking.send();

function functionparking(response) {
    var arrparking = JSON.parse(response);
    var apark;
    var outparking = "<div class='container'><div class='row'>";
    for(apark = 0; apark < 6; apark++) {

        outparking += "<div class='col-md-4'><div class='card-columns'><div class='card card-block'><h3 class='card-title'>" +
            arrparking[apark].description +
            "</h3><br class='card-text'>" +
            "<strong><span class='adressparking'> </span></strong>" +
            arrparking[apark].address +
            "<br>" +

            " There are <strong class='rood'>" +
            arrparking[apark].parkingStatus.availableCapacity +
            "</strong> of the <strong class='rood'>" +
            arrparking[apark].parkingStatus.totalCapacity +
            "</strong> places available <br>" +
            " <small><span class='lastupdate'> " +

        arrparking[apark].lastModifiedDate +


        "</span></small>" +
            "</p>" +

            "</a></div></div></div>";

    }
    outparking += "</div></div>";
    document.getElementById("idparkeren").innerHTML = outparking;



}




$(document).ready(function () {


    $(document).ready(function () {
        $("#postcontent").submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "subscribe.php",
                data: $("#postcontent").serialize(),
                success: function (response) {
                    $('[name="email"]').val('');
                    // alert(response); // FOR ACTUAL RESPONSE
                    alert('Thanks for subscribing us');
                }
            });
            e.preventDefault();
        });

    });



    $('#openBtn').click(function () {
        $('#myModal').modal({
            show: true
        })
    });

    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });





});